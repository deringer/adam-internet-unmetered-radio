MUSIC_PREFIX = "/music/adaminternetunmeteredradio"

NAME = L('Title')

# Artwork
ART  = 'adam-internet-white.jpg'
ICON = 'adam-internet-icon.png'

####################################################################################################
def Start():
    ## make this plugin show up in the 'Music' section
    Plugin.AddPrefixHandler(MUSIC_PREFIX, adamInternetUnmeteredStreamsMenu, NAME, ICON, ART)

    Plugin.AddViewGroup("InfoList", viewMode="InfoList", mediaType="items")
    Plugin.AddViewGroup("List", viewMode="List", mediaType="items")

    MediaContainer.title1 = NAME
    MediaContainer.viewGroup = "List"
    MediaContainer.art = R(ART)
    DirectoryItem.thumb = R(ICON)
    VideoItem.thumb = R(ICON)
####################################################################################################
def ValidatePrefs():
    radio_xml = Prefs['radio_xml']

    if( radio_xml ):
        return MessageContainer(
            "Success",
            "Loaded unmetered radio feed location"
        )
    else:
        return MessageContainer(
            "Error",
            "Could not identify unmetered feed location"
        )
####################################################################################################
def getUnmeteredStreamsXML():
    data = XML.ElementFromURL( Prefs['radio_xml'] )

    return data
####################################################################################################    
def adamInternetUnmeteredStreamsMenu():
    dir = MediaContainer( viewGroup="InfoList" )
    
    data = getUnmeteredStreamsXML()

    for thisItem in data.xpath( '//item' ):
        thisTitle   = thisItem.xpath( './title' )[0].text
        thisLink    = thisItem.xpath( './link' )[0].text

        dir.Append( TrackItem( thisLink, title=thisTitle, thumb=ICON ) )
        
    return dir